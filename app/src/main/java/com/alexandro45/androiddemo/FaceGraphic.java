/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alexandro45.androiddemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.Landmark;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic extends GraphicOverlay.Graphic {
    //private static final float FACE_POSITION_RADIUS = 10.0f;
    //private static final float ID_TEXT_SIZE = 40.0f;
    //private static final float ID_Y_OFFSET = 50.0f;
    //private static final float ID_X_OFFSET = -50.0f;
    //private static final float BOX_STROKE_WIDTH = 5.0f;

    private static final int COLOR_CHOICES[] = {
        Color.BLUE,
        Color.CYAN,
        Color.GREEN,
        Color.MAGENTA,
        Color.RED,
        Color.WHITE,
        Color.YELLOW
    };
    private static int mCurrentColorIndex = 0;

    //private Paint mFacePositionPaint;
    //private Paint mIdPaint;
    //private Paint mBoxPaint;
    private Paint mEyePaintL;
    private Paint mEyePaintR;

    private volatile Face mFace;
    //private int mFaceId;
    //private float mFaceHappiness;
    private Bitmap glassesOiginal;
    private Bitmap temp;
    private int width, height;
    private int pp;

    private Map<Integer, PointF> mPreviousProportions = new HashMap<>();

    FaceGraphic(GraphicOverlay overlay, Context c) {
        super(overlay);

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        /*mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);

        mIdPaint = new Paint();
        mIdPaint.setColor(selectedColor);
        mIdPaint.setTextSize(ID_TEXT_SIZE);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);*/

        mEyePaintL = new Paint();
        mEyePaintL.setColor(selectedColor);

        mEyePaintR = new Paint();
        mEyePaintR.setColor(selectedColor);

        try {
            glassesOiginal = BitmapFactory.decodeStream(c.getAssets().open("pixel_glasses.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        btm = glassesOiginal;
    }

    void setId(int id) {
        //mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face,int pp) {
        mFace = face;
        this.pp = pp;
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        try {
            Face face = mFace;
            if (face == null) {
                return;
            }

            updatePreviousProportions(face);
            PointF l = getLandmarkPosition(face, Landmark.LEFT_EYE);
            PointF r = getLandmarkPosition(face,Landmark.RIGHT_EYE);

            //int width = (int) Math.sqrt((int) (r.x - l.x)^2) + 20;
            width = (int) (l.x - r.x) + pp;
            height = 206 / (1024/width);
            //Log.i("deb",(int) (Math.abs((r.x - l.x) + 50)) + " " + (int) (Math.abs(Math.sqrt((int) ((l.y - r.y))^2)) + 20));
            Log.d("deb",r.x + " " + l.x);
            Log.d("deb",width + " " + height);
            temp = Bitmap.createScaledBitmap(glassesOiginal,width,height,false);
            // Draws a circle at the position of the detected face, with the face's track id below.
            //float x = translateX(face.getPosition().x + face.getWidth() / 2);
            //float y = translateY(face.getPosition().y + face.getHeight() / 2);
            //canvas.drawCircle(x, y, FACE_POSITION_RADIUS, mFacePositionPaint);
            //canvas.drawText("id: " + mFaceId, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
            canvas.drawBitmap(temp,r.x - pp/2,r.y - pp/4,mEyePaintL);
            canvas.saveLayer(new RectF(0,0,canvas.getWidth(),canvas.getHeight()),mEyePaintL,Canvas.ALL_SAVE_FLAG);
            //btm = temp;
            glassX = (int) (r.x - pp/2);
            glassY = (int) (r.y - pp/4);
            glassWidth = width;
            glassHeight = height;
            /* стішок :
            Село і грубі праніки,
            Сало з часником,
            Дід Петро долину косит,
            Тьотя Галя сатає.
             */

        /*canvas.drawCircle(l.x,l.y,10.0f,mEyePaintL);
        canvas.drawCircle(r.x,r.y,10.0f,mEyePaintR);*/
            //canvas.drawText("happiness: " + String.format("%.2f", face.getIsSmilingProbability()), x - ID_X_OFFSET, y - ID_Y_OFFSET, mIdPaint);
            //canvas.drawText("right eye: " + String.format("%.2f", face.getIsRightEyeOpenProbability()), x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
            //canvas.drawText("left eye: " + String.format("%.2f", face.getIsLeftEyeOpenProbability()), x - ID_X_OFFSET*2, y - ID_Y_OFFSET*2, mIdPaint);

            // Draws a bounding box around the face.
            //float xOffset = scaleX(face.getWidth() / 2.0f);
            //float yOffset = scaleY(face.getHeight() / 2.0f);
            //float left = x - xOffset;
            //float top = y - yOffset;
            //float right = x + xOffset;
            //float bottom = y + yOffset;
            //canvas.drawRect(left, top, right, bottom, mBoxPaint);
        } catch (Exception e) {
            System.out.println("вобше похуй");
            canvas.drawCircle(20f,20f,10f,mEyePaintR);
        }
    }

    private void updatePreviousProportions(Face face) {
        for (Landmark landmark : face.getLandmarks()) {
            PointF position = landmark.getPosition();
            float xProp = (position.x - face.getPosition().x) / face.getWidth();
            float yProp = (position.y - face.getPosition().y) / face.getHeight();
            mPreviousProportions.put(landmark.getType(), new PointF(xProp, yProp));
        }
    }

    private PointF getLandmarkPosition(Face face, int landmarkId) {
        for (Landmark landmark : face.getLandmarks()) {
            if (landmark.getType() == landmarkId) {
                return landmark.getPosition();
            }
        }

        PointF prop = mPreviousProportions.get(landmarkId);
        if (prop == null) {
            return null;
        }

        float x = face.getPosition().x + (prop.x * face.getWidth());
        float y = face.getPosition().y + (prop.y * face.getHeight());
        return new PointF(x, y);
    }
}
